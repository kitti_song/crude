import React, { Component } from 'react'
import { withFirebase } from 'react-redux-firebase'
import Popup from 'react-popup'
import { Button, Input, Row, Col, Label } from 'reactstrap'
let obj
class addEmp extends Component {
  constructor(props) {
    super(props)

    this.state = {

      firstname: '',
      lastname: '',
      age: '',
      salary: '',
      gender: 'Male'
    }
    obj = this
  }


  saveData = () => {
    const { firebase } = this.props
    const { firstname, lastname, age, salary, gender } = this.state

    console.log(firstname + ' ' + lastname + ' ' + age + ' ' + salary + ' ' + gender)
    if (firstname != '' && lastname != '' && age != '' && gender != '') {
      firebase.push('/employee', {
        firstname: firstname,
        lastname: lastname,
        age: parseInt(age),
        salary: parseFloat(salary),
        gender: gender,
        done: false,
        timestamp: firebase.database.ServerValue.TIMESTAMP
      }).then(() => {
        alert("เพิ่มพนักงานสำเร็จ")
        this.setState({ content: '' })
      }).catch(error => {
        alert(`ไม่สามารถเพิ่มพนักงานได้ (${error.message})`)
      })
    }
    else {
      alert('กรุณากรอกข้อมูลให้ครบถ้วน')
    }
  }
  addEmp() {
    Popup.create({
      title: 'เพิ่มพนักงาน',
      content: <Row>
        <Col md={6} xs={6}>
          <Label>Firstname</Label>
          <Input type='text' onChange={(event) => this.setState({ firstname: event.target.value })} />
        </Col>

        <Col md={6} xs={6}>
          <Label>Lastname</Label>
          <Input type='text' onChange={(event) => this.setState({ lastname: event.target.value })} />
        </Col>

        <Col md={4} xs={4}>
          <Label>Age</Label>
          <Input type='number' onChange={(event) => this.setState({ age: event.target.value })} />

        </Col>

        <Col md={4} xs={4}>
          <Label>Salary</Label>
          <Input type='number' onChange={(event) => this.setState({ salary: event.target.value })} />
        </Col>

        <Col md={4} xs={4}>
          <Label>Gender</Label>
          <Input type='select' onChange={(event) => this.setState({ gender: event.target.value })}>
            <option value='Male'>Male</option>
            <option value='Femail'>Female</option>
          </Input>
        </Col>
      </Row>,
      
      className: 'alert',

      buttons: {
        right: [{
          text: 'บันทึก',
          className: 'success',
          action: function () {
            obj.saveData()
            Popup.close();
          }
        }]
      }
    }, true);
  }

  render() {
    return (
      <div>
        <Button color='primary' onClick={() => this.addEmp()}>Add Employee</Button>

      </div>
    )
  }
}

export default withFirebase(addEmp)
