import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory from 'react-bootstrap-table2-filter'
import { Button, Row, Col, Input, Label } from 'reactstrap'
import Popup from 'react-popup'
let obj
class ListEmp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firstname: '',
      lastname: '',
      age: '',
      salary: '',
      gender: 'Male',
      showEmp: [

        {
          dataField: 'value.firstname',
          text: 'FIRSTNAME',
          headerStyle: { textAlign: 'center'}

        },
        {
          dataField: 'value.lastname',
          text: 'LASTNAME',
          headerStyle: { textAlign: 'center' }
        },
        {
          dataField: 'value.age',
          text: 'AGE',
          headerStyle: { textAlign: 'center'},
          style: { textAlign: 'center' },
          headerAttrs:{
            width:60
          }
        },
        {
          dataField: 'value.gender',
          text: 'GENDER',
          headerStyle: { textAlign: 'center' },
          style: { textAlign: 'center' }
        },
        {
          dataField: 'value.salary',
          text: 'SALARY',
          headerStyle: { textAlign: 'center' },
          style: { textAlign: 'right' }
        },
        {
          dataField: 'Action',
          text: 'Actions',
          headerStyle: { textAlign: 'center' },
          style: {
            textAlign: 'center'
          },
          formatter: this.actions.bind(this),
          headerAttrs: {
            width: '30%'
          }
        },
      ],
    }
    obj = this
  }
  actions(row, cell) {
    console.log(row)
    //console.log(cell)
    return (
      <div>
        <Button onClick={() => this.updateEmp(cell.key)} style={{ margin: 10, width: 70, backgroundColor: 'orange' }}>แก้ไข</Button>
        <Button onClick={() => this.handleRemoveItem(cell.key)} style={{ width: 70, backgroundColor: 'red' }}>ลบ</Button>
      </div>
    )
  }
  saveData = (key) => {
    const { firebase } = this.props
    const { firstname, lastname, age, salary, gender } = this.state

    console.log(firstname + ' ' + lastname + ' ' + age + ' ' + salary + ' ' + gender)
    if (firstname != '' && lastname != '' && age != '' && salary != '' && gender != '') {
      firebase.update(`/employee/${key}`,
        {
          firstname: firstname,
          lastname: lastname,
          age: age,
          salary: salary,
          gender: gender
        }).then(() => {
          alert("อัพเดทข้อมูลสำเร็จ")
          Popup.close();
        }).catch(error => {
          alert(`ไม่สามารถอัพเดทข้อมูลได้ (${error.message})`)
        })
    }
    else {
      alert('กรุณากรอกข้อมูลให้ครบถ้วน')
    }
  }
  updateEmp(key) {
    let mySpecialPopup = Popup.register({
      title: 'เพิ่มพนักงาน',
      content: <Row>
        <Col md={6} xs={6}>
          <Label>Firstname</Label>
          <Input type='text' onChange={(event) => this.setState({ firstname: event.target.value })} />
        </Col>

        <Col md={6} xs={6}>
          <Label>Lastname</Label>
          <Input type='text' onChange={(event) => this.setState({ lastname: event.target.value })} />
        </Col>

        <Col md={4} xs={4}>
          <Label>Age</Label>
          <Input type='number' onChange={(event) => this.setState({ age: event.target.value })} />

        </Col>

        <Col md={4} xs={4}>
          <Label>Salary</Label>
          <Input type='number' onChange={(event) => this.setState({ salary: event.target.value })} />
        </Col>

        <Col md={4} xs={4}>
          <Label>Gender</Label>
          <Input type='select' onChange={(event) => this.setState({ gender: event.target.value })}>
            <option value='Male'>Male</option>
            <option value='Femail'>Female</option>
          </Input>
        </Col>
      </Row>,

      className: 'alert',

      buttons: {
        right: [{
          text: 'บันทึก',
          className: 'success',
          action: function () {
            obj.saveData(key)
          }
        }]
      }
    });
    Popup.queue(mySpecialPopup);
  }
  componentWillMount() {
    console.log(this.props.employee)
  }
  handleRemoveItem = (key) => {
    const { firebase } = this.props

    firebase.remove(`/employee/${key}`).then(() => {
      alert("ลบข้อมูลสำเร็จ")
    }).catch(error => {
      alert(`ไม่สามารถลบข้อมูลได้ (${error.message})`)
    })
  }

  render() {
    const { employee } = this.props

    if (!isLoaded(employee)) {
      return 'กำลังโหลด...'
    }

    if (isEmpty(employee)) {
      return 'รายการงานว่าง'
    }

    return (

      <div style={{ justifyContent: 'center', display: 'inline-block', maxWidth: 800 }}>
        <BootstrapTable

          keyField='id'
          data={this.props.employee}
          columns={this.state.showEmp}
          filter={filterFactory()}
          hover condensed
        />
      </div>
    )
  }
}

function mapStateToProps({ firebase }) {
  return {
    employee: firebase.ordered.employee
  }
}

const enhance = compose(firebaseConnect([
  { path: '/employee', queryParams: ['orderByChild=timestamp'] }
]), connect(mapStateToProps))

export default enhance(ListEmp)
