import React from 'react'

import AddEmp from '../containers/AddEmp'
import ListEmp from '../containers/ListEmp'
// import 'bootstrap/dist/css/bootstrap.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import '../Styles/popup.css'
import '../../node_modules/bootstrap/dist/css/bootstrap.css'
import Popup from 'react-popup';
const App = () => (
  <div style={{margin:20,textAlign:'center'}}>
    <Popup
      className="mm-popup"
      btnClass="mm-popup__btn"
      closeBtn={true}
      closeHtml={null}
      defaultOk="Ok"
      defaultCancel="Cancel"
      // wildClasses={false}
      escToClose={true} />
    <AddEmp />
    <ListEmp />
  </div>
)

export default App
